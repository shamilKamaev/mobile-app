import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    navMenuActive: false,
    menuItems: [], menuItemsLoaded: false,
    newsItems: [], newsItemsLoaded: false,
    shopingCartList: []
  },
  getters: {

  },
  mutations: {
    setBascketStatus(state, payload) {
      Vue.set(state.menuItems[payload.categorypath].items[payload.itempath], 'bascketStatus', true)
      Vue.set(state.menuItems[payload.categorypath].items[payload.itempath], 'itemsCount', 1)
    }
  },
  actions: {

  }
})
