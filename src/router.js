import Vue from 'vue'
import Router from 'vue-router'

// views
import Registration from './views/Registration'
import Dashboard from './views/Dashboard'
import MenuCategories from './views/MenuCtegories'
import MenuItems from './views/MenuItems'
import News from './views/News'
import Booking from './views/Booking'
import Contacts from './views/Contacts'
import Bascket from './views/Bascket'

Vue.use(Router)
export default new Router({
  mode: process.env.CORDOVA_PLATFORM ? 'hash' : 'history',

  routerBasePath:  '/sinebruhovapp/',

  routes: [
    {
      path: '/sinebruhovapp/',
      component: Registration,
      name: 'Registration',
      meta: {
        navTable: false
      }
    },
    {
      path: '/sinebruhovapp/Dashboard',
      component: Dashboard,
      name: 'Dashboard',
      meta: {
        navTable: false
      }
    },
    {
      path: '/sinebruhovapp/MenuCtegories',
      component: MenuCategories,
      name: 'MenuCategories',
      meta: {
        title: 'Меню',
        navTable: true,
        backPath: '/sinebruhovapp/'
      }
    },
    {
      path: '/sinebruhovapp/MenuItems/:id',
      component: MenuItems,
      name: 'MenuItems',
      meta: {
        navTable: true,
        backPath: '/sinebruhovapp/MenuCtegories'
      }
    },
    {
      path: '/sinebruhovapp/News',
      component: News,
      name: 'News',
      meta: {
        title: 'События и акции',
        navTable: true,
        backPath: '/sinebruhovapp/'
      }
    },
    {
      path: '/sinebruhovapp/Booking',
      component: Booking,
      name: 'Booking',
      meta: {
        title: 'Бронирование',
        navTable: true,
        backPath: '/sinebruhovapp/'
      }
    },
    {
      path: '/sinebruhovapp/Contacts',
      component: Contacts,
      name: 'Contacts',
      meta: {
        title: 'Контакты',
        navTable: true,
        backPath: '/sinebruhovapp/'
      }
    },
    {
      path: '/sinebruhovapp/Bascket',
      component: Bascket,
      name: 'Bascket',
      meta: {
        title: 'Корзина',
        navTable: true,
        backPath: '/sinebruhovapp/MenuCtegories'
      }
    },
  ],
  
  scrollBehavior() {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve({ x: 0, y: 0 })
      }, 500)
    })
  }
})