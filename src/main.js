import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// global plugins
import hammer from '../node_modules/hammerjs/hammer'
import VueAnime from 'vue-animejs';
// axios
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(VueAxios, axios.create({ baseURL: `http://krater.big-d.ru/` }))
Vue.use(VueAnime)
// global components
import BtnDefault from './components/ButtonDefault.vue'
Vue.component('BtnDefault', BtnDefault)



new Vue({
  hammer,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
